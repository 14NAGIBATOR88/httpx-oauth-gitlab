import re

import pytest
import respx
from httpx import Response

from httpx_oauth.clients.gitlab import PROFILE_ENDPOINT, GitLabOAuth2
from httpx_oauth.errors import GetIdEmailError

client = GitLabOAuth2("CLIENT_ID", "CLIENT_SECRET")


def test_gitlab_oauth2():
    assert client.authorize_endpoint == "https://gitlab.com/oauth/authorize"
    assert client.access_token_endpoint == "https://gitlab.com/oauth/token"
    assert client.refresh_token_endpoint == "https://gitlab.com/oauth/token"
    assert client.revoke_token_endpoint == "https://gitlab.com/oauth/revoke"
    assert client.base_scopes == ["read_user", "email"]
    assert client.name == "gitlab"


profile = {
    "id": "12345678",
    "username": "Bek",
    "name": "kekich",
    "state": "active",
    "avatar_url": "https://secure.gravatar.com/avatar/asfasasd",
    "email": "baranbl@ebet.ik",
}


class TestGitLabGetIdEmail:
    @pytest.mark.asyncio
    @respx.mock
    async def test_success(self, get_respx_call_args):
        request = respx.get(re.compile(f"^{PROFILE_ENDPOINT}")).mock(
            return_value=Response(200, json=profile)
        )

        user_id, user_email = await client.get_id_email("TOKEN")
        _, headers, _ = await get_respx_call_args(request)

        assert headers["Authorization"] == "Bearer TOKEN"
        assert headers["Accept"] == "application/json"
        assert user_id == "12345678"
        assert user_email == "baranbl@ebet.ik"

    @pytest.mark.asyncio
    @respx.mock
    async def test_error(self):
        respx.get(re.compile(f"^{PROFILE_ENDPOINT}")).mock(
            return_value=Response(400, json={"error": "message"})
        )

        with pytest.raises(GetIdEmailError) as excinfo:
            await client.get_id_email("TOKEN")

        assert type(excinfo.value.args[0]) == dict
        assert excinfo.value.args[0] == {"error": "message"}
